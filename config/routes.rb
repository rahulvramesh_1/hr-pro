Rails.application.routes.draw do
  get 'dashboard/index'

  get 'employees/list'

  get 'clients/list'

  root 'login_page#login'
  get 'application/hello'
end
